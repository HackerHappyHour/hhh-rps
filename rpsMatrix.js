module.exports = {
  rock: {
	beats: ['scissors','lizard', 'klingon']
  },
  paper: {
	beats: ['rock', 'spock', 'enterprise']
  },
  scissors: {
	beats: ['paper', 'lizard', 'klingon']
  },
  lizard: {
    beats: ['paper', 'spock', 'enterprise']
  },
  spock: {
    beats: ['rock','scissors', 'enterprise']
  },
  enterprise: {
	beats: ['scissors', 'klingon', 'rock']
  },
  klingon: {
	beats: ['spock', 'paper', 'lizard']
  }
}
