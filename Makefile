RPSTHROW=${rps}
run: Dockerfile
	docker run -it --rm -v `pwd`:/rps rps $(RPSTHROW)

build: Dockerfile
	docker build -t rps .
