Follow the docs in rps.js, or if you want to run with docker, save both rps.js and Dockerfile in the same directory, run:

## How to Play

### git/node

```
$ git clone https://gitlab.com/HackerHappyHour/hhh-rps.git /rps
$ cd /rps
$ node /rps/rps.js rock
The computer chose scissors, and...
When rock faces scissors, rock wins!
```

### Docker

```bash
$ docker build -t rps .
$ docker run -it --rm rps rock 
The computer chose scissors, and...
When rock faces scissors, rock wins!
```

### make

If you have docker installed, but want to use shorter commands, you can utilize
`make`, to substitue docker commands:

```bash
$ make build//builds docker image
$ make run rps=rock
The computer chose scissors, and...
When rock faces scissors, rock wins!
```

## Contributing

```bash
$ git clone https://gitlab.com/HackerHappyHour/hhh-rps.git /rps
```

### Using docker instead of native node

Make sure you've built the image at least once using the usage instructions above.
You only need to rebuild the image if you make changes to the Dockerfile (changes to
app code only require rebuilding once you're ready to distribute the app)

Use your editor of choice to edit the files in your local clone, and then use
docker to run the new ccode using:

```bash
$ docker run -it --rm -v `pwd`:/rps rps <rps arguments>
```

The docker image enters using the `node` command, so anything you pass to the docker
image after the `rps` will be the same as running `node <rps arguments`.
