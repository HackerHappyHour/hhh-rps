const rpsThrows = process.argv.slice(2)
const rpsMatrix = require('./rpsMatrix')
const rpsMatrixKeys = Object.keys(rpsMatrix)

ensureMatrixIsValid()
generateComputerThrow()
processRpsThrows()

/* Private functions for processing RPS Matches */

function generateComputerThrow(){
  var computerThrow = rpsMatrixKeys[Math.floor(Math.random() * rpsMatrixKeys.length)]
  rpsThrows.push(computerThrow)
}

function ensureMatrixIsValid(){
  rpsMatrixKeys.forEach(key => ensureMatrixKeyIntegrity(key))
}

// ensure rpsMatrix keys don't beat themselves or result in
// a double-failure
function ensureMatrixKeyIntegrity(key){
  let keyBeats = rpsMatrix[key]['beats']

  if(keyBeats.indexOf(key) > -1){
	exitRpsMatchWithError(`ERROR: ${key} cannot beat itself!`)
  }

  keyBeats.forEach(x =>{
 	if(rpsMatrix[x]['beats'].indexOf(key) > 1) {
      exitRpsMatchWithError(`ERROR: ${key} and ${x} result in Mutually Assured Destruction`)
	}
  })  

}

function processRpsThrows(rpsMatrix, rpsThrows){
  // make sure all throws are valid
  validateRpsMatch()

  // determine winner
  var rpsMatchWinner = determineRpsMatchWinner();

  // display results
  displayRpsMatchResults(rpsMatchWinner);
}


// ensures RPS Matchthrows are valid
function validateRpsMatch(){
  rpsThrows.forEach(arg => ensureRpsThrowIsValid(arg))

  if (rpsThrows.length != 2){
	exitRpsMatchWithError('ERROR: Exactly 2 throws must be provided')
  }

  if (rpsThrows[0] === rpsThrows[1]){
	exitRpsMatchWithError('NO WINNER, both throws are the same')
  }
}

// ensure a given thrown is valid
function ensureRpsThrowIsValid(arg){
  if (rpsMatrix[arg] === undefined) {
  	exitRpsMatchWithError(`ERROR: ${arg} is not a valid throw`);
  }
}

// determine the winner of the match
function determineRpsMatchWinner(){
  //if (rpsMatrix[rpsThrows[0]]['beats'] == rpsThrows[1])
  if (rpsMatrix[rpsThrows[0]]['beats'].indexOf(rpsThrows[1]) > -1 ){
    return rpsThrows[0]	
  } else {
    return rpsThrows[1]
  }
}

function displayRpsMatchResults(winner){
  console.log(`The computer chose ${rpsThrows[1]}, and...`)
  console.log(`When ${rpsThrows[0]} faces ${rpsThrows[1]}, ${winner} wins!`)
}

function exitRpsMatchWithError(message){
  console.error(message)
  process.exit(1)
}
