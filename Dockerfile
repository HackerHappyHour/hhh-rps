FROM node:latest
RUN mkdir /rps
COPY *.js /rps/

ENTRYPOINT ["node", "/rps/rps.js"]
